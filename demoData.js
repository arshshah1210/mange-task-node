const store = {
  tasks: {
    task1: {
      id: 1,
      title: 'title 1',
      description: 'description 1',
      createdAt: 1000000,
      colId: 'col1',
      comments: ['Comment 1', 'Comment 2'],
    },
  },
  columns: {
    col1: {
      id: 1,
      taskId: ['task1'],
      title: 'Column 1',
    },
  },
  colOrder: ['col1'],
};

[
  {
    title,
    id,
    taks: [{ task_details }],
  },
];
