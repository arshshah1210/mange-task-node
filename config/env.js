// Databse configuration as of sequelize.
const env = {
  host: 'localhost',
  username: 'root',
  password: '',
  database: 'manage_tasks',
  dialect: 'mysql',
};

module.exports = env;
