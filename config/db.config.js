const { Sequelize } = require('sequelize');
const env = require('./env');
const tasks = require('../model/task.model');
const column = require('../model/column.model');

// Sequelize instance to connect to database.
const sequelize = new Sequelize(env.database, env.username, env.password, {
  host: env.host,
  dialect: env.dialect,
});

// Checking for database connection.
const dbCon = async () => {
  try {
    await sequelize.authenticate();
    console.log('Connection has been established successfully.');
  } catch (error) {
    console.error('Unable to connect to the database:', error.message);
  }
};

// Intializing models/tables with sequelize instance.
const Task = tasks(sequelize);
const Column = column(sequelize);
// console.log('sequelize', sequelize);

/* Creating foreign key instance for task table. */

// Options for setting foreign key.
const options = {
  foreignKey: {
    name: 'colId',
  },
  onDelete: 'CASCADE',
  onUpdate: 'RESTRICT',
};

// Setting relation between tables (many-to-one).
Column.hasMany(Task, options);
Task.belongsTo(Column, options);

module.exports = { dbCon, sequelize };
