const express = require('express');
const {
  findAll,
  update,
  findById,
  destroy,
  create,
} = require('../controller/column.controller');

const columnsRoute = express.Router();

// Create column.
columnsRoute.post('/', create);

// Get all columns.
columnsRoute.get('/', findAll);

// Get column by id.
columnsRoute.get('/:id', findById);

// Update column by id.
columnsRoute.put('/:id', update);

// Delete column by id.
columnsRoute.delete('/:id', destroy);

module.exports = columnsRoute;
