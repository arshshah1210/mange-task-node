const express = require('express');
const {
  findAll,
  findById,
  update,
  destroy,
  create,
} = require('../controller/task.controller');

const tasksRouter = express.Router();

// Create task.
tasksRouter.post('/', create);

// Get all tasks.
tasksRouter.get('/', findAll);

// Get task by id.
tasksRouter.get('/:id', findById);

// Update task by id.
tasksRouter.put('/:id', update);

// Delete task by id.
tasksRouter.delete('/:id', destroy);

module.exports = tasksRouter;
