const { sequelize } = require('../config/db.config');
const { getColumnsData } = require('../misc/helper');

// Getting task and column model from sequelize instance in models.
const Tasks = sequelize.models.task;
const Columns = sequelize.models.column;

// Common function getting column by id.
const getColumnById = async (id) => {
  /* Get columns according to id and if no columns found returns null */
  const row = await Columns.findOne({ include: Tasks, where: { id } });

  return row;
};

// Creating Column.
exports.create = async (req, res) => {
  try {
    const column = await Columns.create(req.body);

    res.send(column);
  } catch (err) {
    res.status(500).send(`ERROR ${err.message}`);
  }
};

// Finding all Columns
exports.findAll = async (req, res) => {
  try {
    const rows = await Columns.findAll({
      include: Tasks, // Left outer join on task table.
      order: ['id'], // Ascend order by column id
    });

    // Format rows according to redux store.
    const columns = getColumnsData(rows);

    res.send(columns);
  } catch (err) {
    res.status(500).send(`ERROR ${err.message}`);
  }
};

// Finding single column by id.
exports.findById = async (req, res) => {
  const { id } = req.params;

  try {
    const row = await getColumnById(id);

    // Checking for empty rows.
    if (row) {
      // Format rows according to redux store.
      const column = getColumnsData([row]);

      res.send(column);
    } else {
      res.status(400).send(`No column found with id ${id}`);
    }
  } catch (err) {
    res.status(500).send(`ERROR ${err.message}`);
  }
};

// Updating Column.
exports.update = async (req, res) => {
  const { id } = req.params;

  try {
    const row = await getColumnById(id);

    // If row found then update it otherwise not found.
    if (row) {
      const newColumn = req.body;

      // Updating column title by id.
      await Columns.update(newColumn, { where: { id } });

      res.send(newColumn);
    } else {
      res.status(400).send(`No column found with id ${id}`);
    }
  } catch (err) {
    res.status(500).send(`ERROR ${err.message}`);
  }
};

// Deleting column by id.
exports.destroy = async (req, res) => {
  const { id } = req.params;

  try {
    const row = await getColumnById(id);

    // If row found then delete it otherwise not found.
    if (row) {
      await Columns.destroy({ where: { id } });

      res.send(`Column with id ${id} was successfully deleted`);
    } else {
      res.status(400).send(`No column found with id ${id}`);
    }
  } catch (err) {
    res.status(500).send(`ERROR ${err.message}`);
  }
};
