const { sequelize } = require('../config/db.config');
const { getTasksData } = require('../misc/helper');

// Getting task model from sequelize instance in models.
const Tasks = sequelize.models.task;

// Common function getting task by id.
const getTaskById = async (id) => {
  /* Get task according to id and if no task found returns null */
  const row = await Tasks.findByPk(id);

  return row;
};

// Creating Task.
exports.create = async (req, res) => {
  try {
    const task = await Tasks.create(req.body);

    res.send(task);
  } catch (err) {
    res.status(500).send(`ERROR ${err.message}`);
  }
};

// Getting all the tasks
exports.findAll = async (req, res) => {
  try {
    const rows = await Tasks.findAll();

    // Format rows according to redux store.
    const tasks = getTasksData(rows);

    res.send(tasks);
  } catch (err) {
    res.status(500).send(`Error ->>> ${err.message}`);
  }
};

// Getting task by id.
exports.findById = async (req, res) => {
  const { id } = req.params;

  try {
    // Getting task by primary key.
    const row = await getTaskById(id);

    // Check for task exist else empty task.
    if (row) {
      const task = getTasksData([row]);
      res.send(task);
    } else {
      res.send(`No task found with id ${id}`);
    }
  } catch (err) {
    res.status(500).send(`Error ->>> ${err.message}`);
  }
};

// Udpating task by id.
exports.update = async (req, res) => {
  const { id } = req.params;

  try {
    const row = await getTaskById(id);

    // If row found then update it otherwise not found.
    if (row) {
      const newTask = req.body;

      // Updates the parameter according to the propety specified.
      await Tasks.update(newTask, { where: { id } });

      res.send(newTask);
    } else {
      res.status(400).send(`No task found with id ${id}`);
    }
  } catch (err) {
    res.status(500).send(`ERROR ${err.message}`);
  }
};

// Deleting/Droping Task by id.
exports.destroy = async (req, res) => {
  const { id } = req.params;

  try {
    const row = await getTaskById(id);

    if (row) {
      await Tasks.destroy({ where: { id } });

      res.send(`Task with id ${id} was successfully deleted`);
    } else {
      res.status(400).send(`No task found with id ${id}`);
    }
  } catch (err) {
    res.status(500).send(`ERROR ${err.message}`);
  }
};
