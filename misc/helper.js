const getColumnsData = (data) => {
  const columns = {};

  data.forEach((row) => {
    columns[`col${row.id}`] = {
      id: row.id,
      title: row.title,
      taskId: row.tasks.length ? row.tasks.map((task) => task.id) : [],
    };
  });

  return columns;
};

const getTasksData = (data) => {
  const tasks = {};

  data.forEach((row) => {
    tasks[`task${row.id}`] = {
      id: row.id,
      title: row.title,
      description: row.description,
      createdAt: row.createdAt,
      colId: row.colId,
    };
  });

  return tasks;
};

module.exports = {
  getColumnsData,
  getTasksData,
};
