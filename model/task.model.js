const { DataTypes } = require('sequelize');

// Passing Sequelize instance sequelize.
module.exports = (sequelize) => {
  // `sequelize.define` also returns the model
  const Task = sequelize.define(
    'task',
    {
      title: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      description: {
        type: DataTypes.STRING,
      },
      createdAt: {
        type: DataTypes.BIGINT(11),
        allowNull: false,
      },
      colId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
    },
    {
      // Remove the default columns by sequelize 'createdAt' and 'updatedAt'.
      timestamps: false,
    }
  );

  return Task;
};
