const { DataTypes } = require('sequelize');

// Passing Sequelize instance sequelize.
module.exports = (sequelize) => {
  // `sequelize.define` also returns the model
  const Column = sequelize.define(
    'column',
    {
      title: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      // Remove the default columns by sequelize 'createdAt' and 'updatedAt'.
      timestamps: false,
    }
  );

  return Column;
};
