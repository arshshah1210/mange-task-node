const express = require('express');

const { dbCon, sequelize } = require('./config/db.config');
const columnsRoute = require('./route/column.route');
const tasksRouter = require('./route/task.route');

const app = express();

/**
 * Get port from environment and store in Express.
 */

const port = process.env.port || 8080;

// Setting CORS Policy.
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

/*
 *Checks what is the current state of the table in the database (which columns it has, what are their data types, etc), and then performs the necessary changes in the table to make it match the model.
 */

// sequelize.sync({ alter: true });

/* MiddleWares */

// Built-in middlewares.
app.use(express.json()); // For parsing and adding json like data to req.body.
app.use(express.urlencoded({ extended: true })); // For parsing and adding form like data to req.body.

// Custom middlewares.
app.use('/api/tasks', tasksRouter);
app.use('/api/columns', columnsRoute);

// Checking for db connection.
dbCon();

// Port listening.
app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
